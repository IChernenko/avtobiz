<!DOCTYPE html>
<html dir="<?php echo $direction; ?>
  " lang="
  <?php echo $lang; ?>
  ">
<head>
  <meta charset="UTF-8" />
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>
  " />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>
  " />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>
  " />
  <?php } ?>
  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>
  " rel="icon" />
  <?php } ?>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>
  " rel="
  <?php echo $link['rel']; ?>
  " />
  <?php } ?>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
  <?php foreach ($styles as $style) { ?>
  <link rel="<?php echo $style['rel']; ?>
  " type="text/css" href="
  <?php echo $style['href']; ?>
  " media="
  <?php echo $style['media']; ?>
  " />
  <?php } ?>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
  <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
  <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
  <?php foreach ($scripts as $script) { ?>
  <script type="text/javascript" src="<?php echo $script; ?>"></script>
  <?php } ?>
  <!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
  <![endif]-->
  <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
  <script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
  <script type="text/javascript">DD_belatedPNG.fix('#logo img');</script>
  <![endif]-->
  <?php if ($stores) { ?>
  <script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
  <?php } ?>

			<link rel="stylesheet" href="catalog/view/javascript/jquery.cluetip.css" type="text/css" />
			<script src="catalog/view/javascript/jquery.cluetip.js" type="text/javascript"></script>
			
			<script type="text/javascript">
				$(document).ready(function() {
				$('a.title').cluetip({splitTitle: '|'});
				  $('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});
				  $('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});
				  $('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});
				  $('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});
				  $('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  
				});
			</script>
			
  <?php echo $google_analytics; ?></head>
<body>
  <div id="container">
    <div id="header">
      <?php if ($logo) { ?>
      <div id="logo">
        <a href="<?php echo $home; ?>
          ">
          <img src="<?php echo $logo; ?>
          " title="
          <?php echo $name; ?>
          " alt="
          <?php echo $name; ?>" /></a>
        <br>
        <!-- <center>
        <a href="/">Интернет магазин запчастей</a>
      </center>
      -->
    </div>
    <?php } ?>
    <div class="ew">
      <a id="spcallmeback_btn_1" class="spcallmeback_raise_btn" href="#spcallmeback_1">
        <!-- <img src="/catalog/view/theme/default/image/zakaz-zvonka.png" alt="img" class="header-call"> -->
      </a>
    </div>
    <img src="/catalog/view/theme/default/image/car.png" alt="img" class="header-car">
    <img src="/catalog/view/theme/default/image/phone3-512.png" alt="img" class="header-tel-img">
    <div class="header-tel-block">
      <div>093 025 44 32</div>
      <div>093 025 44 32</div>
      <div>093 025 44 32</div>
    </div>
    <!-- <div class="phone">Пн-Пт 9:00 до 18:00</div>
  -->

  <!-- <div class="scrb">
    <a href="dostavka">Доставка</a>
    |
    <a href="oplata">Оплата</a>
    |
    <a href="kontakti">Контакты</a>
  </div> -->
  <?php echo $cart; ?></div>
<?php if ($categories) { ?>
<div id="menu">
  <ul>
    <?php foreach ($categories as $category) { ?>
    <li>
      <a href="<?php echo $category['href']; ?>
        ">
        <?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li>
            <a href="<?php echo $category['children'][$i]['href']; ?>
              ">
              <?php echo $category['children'][$i]['name']; ?></a>
          </li>
          <?php } ?>
          <?php } ?></ul>
        <?php } ?></div>
      <?php } ?></li>
    <?php } ?></ul>
</div>
<?php } ?>
  <div class="sear">
    
    <input type="text" id="artnum" value="" maxlength="40" class="inpt" placeholder="Введите номер">
    <!-- <input type="submit" id="search-click" value="dsf" class="sbut" onclick="TDMArtSearch()"> -->
    <input type="image" id="search-click" class="sbut" src="/catalog/view/theme/default/image/search-24.png" alt="Submit Form" />
    <script type="text/javascript">
    function TDMArtSearch(){
      var art = $('#artnum').val();
      if(art!=''){
        art = art.replace(/[^a-zA-Z0-9.-]+/g, '');
        location = '/autoparts/search/'+art+'/';
      }
    }
    $('#search-click').click(function() {
      TDMArtSearch();
    });
    $('#artnum').keypress(function (e){
      if(e.which == 13){ TDMArtSearch(); return false;}
    });
    </script>
  </div>
<div id="notification"></div>