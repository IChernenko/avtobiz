<div class="box">
 <div class="box-heading">
    <?php echo $heading_title; ?>
  </div>

  <div class="box-slide-wrapper">
    <div class="some_blockh" id="slideshow<?php echo $module; ?>">  
      <?php foreach ($products as $product) { ?>
        <div class="item">
            <?php if ($product['thumb']) { ?>
        <div class="image">
          <a href="<?php echo $product['href']; ?>
            ">
            <img src="<?php echo $product['thumb']; ?>
            " alt="
            <?php echo $product['name']; ?>" /></a>
        </div>
        <?php } ?>
        <div class="name">
          <a href="<?php echo $product['href']; ?>
            ">
            <?php echo $product['name']; ?></a>
        </div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old">
            <?php echo $product['price']; ?></span>
          <span class="price-new">
            <?php echo $product['special']; ?></span>
          <?php } ?></div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>
          .png" alt="
          <?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        
       
            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="special-button" id="button-cart" />
        </div>
      <?php } ?>
      </div>
  </div>
  
</div>

 


<!-- <div class="box">
  <div class="box-heading">
    <?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-product" id="slideshow<?php echo $module; ?>">     

      <?php foreach ($products as $product) { ?>
      <div class="item">
        <?php if ($product['thumb']) { ?>
        <div class="image">
          <a href="<?php echo $product['href']; ?>
            ">
            <img src="<?php echo $product['thumb']; ?>
            " alt="
            <?php echo $product['name']; ?>" /></a>
        </div>
        <?php } ?>
        <div class="name">
          <a href="<?php echo $product['href']; ?>
            ">
            <?php echo $product['name']; ?></a>
        </div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old">
            <?php echo $product['price']; ?></span>
          <span class="price-new">
            <?php echo $product['special']; ?></span>
          <?php } ?></div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>
          .png" alt="
          <?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        
       
            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="special-button" id="button-cart" />
           
       
      </div>


      <?php } ?></div>
  </div>
</div>

<script>
  $(document).ready(function() {
  // $('#slideshow<?php echo $module; ?>').nivoSlider();
   $("#slideshow<?php echo $module; ?>").owlCarousel({
      slideSpeed : 300,
      paginationSpeed : 600,
      // singleItem : true,
      autoPlay : true,
      stopOnHover :true
      
      // navigationText  ["prev","next"]

  });
});
</script> -->

<script>
  $(document).ready(function() {
  // $('#slideshow<?php echo $module; ?>').nivoSlider();
   $("#slideshow<?php echo $module; ?>").owlCarousel({
      slideSpeed : 300,
      paginationSpeed : 600,
      items: 6,
      // singleItem : true,
      autoPlay : true,
      stopOnHover :true
      
      // navigationText  ["prev","next"]

  });
});
</script>