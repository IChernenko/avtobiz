<?php $height += 50; ?>
<div class="slideshow" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
  <div class="next"> > </div>
  <div class="prev"> < </div>
  <div id="slideshow<?php echo $module; ?>" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
    <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
    <?php } ?>
  </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	// $('#slideshow<?php echo $module; ?>').nivoSlider();
   $("#slideshow<?php echo $module; ?>").owlCarousel({
      slideSpeed : 300,
      paginationSpeed : 600,
      singleItem : true,
      autoPlay : true,
      stopOnHover :true
      
      // navigationText  ["prev","next"]

  });
});
 var owl = $("#slideshow<?php echo $module; ?>");

 $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });

--></script>