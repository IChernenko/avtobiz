<!DOCTYPE html>
<html dir="<?php echo $direction; ?>
  " lang="
  <?php echo $lang; ?>
  ">
<head>
  <meta charset="UTF-8" />
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>
  " />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>
  " />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>
  " />
  <?php } ?>
  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>
  " rel="icon" />
  <?php } ?>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>
  " rel="
  <?php echo $link['rel']; ?>
  " />
  <?php } ?>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
  <?php foreach ($styles as $style) { ?>
  <link rel="<?php echo $style['rel']; ?>
  " type="text/css" href="
  <?php echo $style['href']; ?>
  " media="
  <?php echo $style['media']; ?>
  " />
  <?php } ?>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
  <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
  <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
  <?php foreach ($scripts as $script) { ?>
  <script type="text/javascript" src="<?php echo $script; ?>"></script>
  <script>

  </script>
  <?php } ?>
  <!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
  <![endif]-->
  <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
  <script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
  <script type="text/javascript">DD_belatedPNG.fix('#logo img');</script>
  <![endif]-->
  <?php if ($stores) { ?>
  <script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
  <?php } ?>
  <?php echo $google_analytics; ?></head>
<body>
<div id="header">
<div id="container-inside">
      <?php if ($logo) { ?>
      <div id="logo">
        <a href="<?php echo $home; ?>
          ">
          <img src="<?php echo $logo; ?>
          " title="
          <?php echo $name; ?>
          " alt="
          <?php echo $name; ?>" /></a>
        <br>
        <!-- <center>
        <a href="/">Интернет магазин запчастей</a>
      </center>
      -->
    </div>
    <?php } ?>
    <div class="ew">
    <!-- <?php var_dump($rates) ?> -->
    
      <a id="spcallmeback_btn_1" class="spcallmeback_raise_btn" href="#spcallmeback_1">
        <!-- <img src="/catalog/view/theme/default/image/zakaz-zvonka.png" alt="img" class="header-call"> -->
      </a>
    </div>
    <div class="header-welcome">
    <!-- <?php echo $currency ?> -->
      <?php echo $text_welcome ?>
    <!--   <a href=""></a>
      <a href="%s">Регистрация</a> -->
      <!-- <?php var_dump($currency_data) ?> -->
    
   

    </div>
    <!-- <?php var_dump($currency_data); ?> -->
    <div class="currency-value">
       <?php foreach ($currency_data as $currency_value) { 
          if ($currency_value['code'] == 'UAH') { ?>
            <div class="currency-number">Курс € <?php echo(round($currency_value['value'], 1));  ?></div>
            <div class="currency-date">
               <!-- <?php echo($currency_value['date_modified']);  ?>  -->
              
              <?php
                $originalDate = $currency_value['date_modified'];
                $arr = explode(' ', $originalDate);
                $newDate = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",$arr[0]);
                echo $newDate;
                echo " ";
                echo $arr[1];
                // $newDate = $arr[2].'-'.$arr[2].'-'.$arr[1];
                
                // $newDate = date("d-m-Y", strtotime($originalDate)); 
                
                // echo $newDate;
              ?>
            </div>
             
             
       <?php
          }
        
      } ?>
    </div>
    <img src="/catalog/view/theme/default/image/car.png" alt="img" class="header-car">
    <img src="/catalog/view/theme/default/image/ICQ-Filled-50.png" alt="img" class="icq">
    <img src="/catalog/view/theme/default/image/phone3-512.png" alt="img" class="header-tel-img">
     <div class="header-icq-block">  
      <div>682173284</div>
    </div>
    <div class="header-tel-block">  
      <div><span>067</span> 583 37 73</div>
      <div><span>093</span> 873 79 33</div> 
      <div><span>095</span> 399 72 37 </div> 
    </div>
    <!-- <div class="phone">Пн-Пт 9:00 до 18:00</div>
  -->

  <!-- <div class="scrb">
    <a href="dostavka">Доставка</a>
    |
    <a href="oplata">Оплата</a>
    |
    <a href="kontakti">Контакты</a>
  </div> -->
  <?php echo $cart; ?>
  </div>
  </div>
  <!-- header end -->
  <?php if ($categories) { ?>
<div id="menu">
<div id="container-inside">
  <ul>
    <?php foreach ($categories as $category) { ?>
    <li>
      <a href="<?php echo $category['href']; ?>
        ">
        <?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li>
            <a href="<?php echo $category['children'][$i]['href']; ?>
              ">
              <?php echo $category['children'][$i]['name']; ?></a>
          </li>
          <?php } ?>
          <?php } ?></ul>
        <?php } ?></div>
      <?php } ?></li>
    <?php } ?></ul>
    </div>
</div>
<?php } ?>
  <div id="container">
  <div class="sear">    
    <input type="text" id="artnum" value="" maxlength="40" class="inpt" placeholder="Введите номер: например: OX188D">
    <!-- <input type="submit" id="search-click" value="dsf" class="sbut" onclick="TDMArtSearch()"> -->
    <input type="image" id="search-click" class="sbut" src="/catalog/view/theme/default/image/search-24.png" alt="Submit Form" />
    <!-- <div class="exapple">Например: OX188D</div> -->
    
  </div>
<div id="notification"></div>


<script type="text/javascript">
    function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
    }
    function getCookie(name) {
      var cookie = " " + document.cookie;
      var search = " " + name + "=";
      var setStr = null;
      var offset = 0;
      var end = 0;
      if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
          offset += search.length;
          end = cookie.indexOf(";", offset)
          if (end == -1) {
            end = cookie.length;
          }
          setStr = unescape(cookie.substring(offset, end));
        }
      }
      return(setStr);
    }
   

    if ( getCookie("animate") != 'header' ) {
      $('#logo').addClass('logo-animate');
      $('.header-tel-block').addClass('header-tel-block-animate');
      $('.header-icq-block').addClass('header-icq-block-animate');     
      $('#header #cart .heading').addClass('heading-animate');
      $('.header-welcome').addClass('header-welcome-animate');
      $('.currency-value').addClass('currency-value-animate');
    } else {
      $('#logo').css('opacity', '1');
    }

    setCookie("animate", "header");
   




    function TDMArtSearch(){
      var art = $('#artnum').val();
      if(art!=''){
        art = art.replace(/[^a-zA-Z0-9.-]+/g, '');
        location = '/autoparts/search/'+art+'/';
      }
    }
    $('#search-click').click(function() {
      TDMArtSearch();
    });
    $('#artnum').keypress(function (e){
      if(e.which == 13){ TDMArtSearch(); return false;}
    });
    </script>